﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Oracle.ManagedDataAccess.Client;
using OracleCommand = Oracle.ManagedDataAccess.Client.OracleCommand;
using OracleConnection = Oracle.ManagedDataAccess.Client.OracleConnection;
using OracleDataAdapter = Oracle.ManagedDataAccess.Client.OracleDataAdapter;
using OracleParameter = Oracle.ManagedDataAccess.Client.OracleParameter;

namespace AI.Provider.Connector
{
    /// <summary>
    /// Grants access to a database
    /// </summary>
    internal class OracleProvider
    {
        /// <summary>
        /// Negociates a connection and gets the data with the default connection string, no parameter is used
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        public OracleProvider(string storedProcedure)
        {
            _connectionString = _defaultConnection;
            GetData(storedProcedure, null, null, null);
        }

        /// <summary>
        /// Negociates a connection and gets the data with the default connection string
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        /// <param name="values">Stored procedure parameter values</param>
        public OracleProvider(string storedProcedure, IReadOnlyList<string> parameters, IReadOnlyList<object> values)
        {
            _connectionString = _defaultConnection;
            GetData(storedProcedure, parameters, values, null);
        }

        /// <summary>
        /// Negociates a connection and gets the data with the default connectiong string, also expects any number of cursors
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        /// <param name="values">Stored procedure parameter values</param>
        /// <param name="cursors">Cursors names should coincide with names on the parameters</param>
        public OracleProvider(string storedProcedure, IReadOnlyList<string> parameters, IReadOnlyList<object> values, IReadOnlyList<string> cursors)
        {
            _connectionString = _defaultConnection;
            GetData(storedProcedure, parameters, values, cursors);
        }

        /// <summary>
        /// Negociates a connection and gets the data with a different connection string
        /// </summary>
        /// <param name="connectionString">Connection String</param>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        /// <param name="values">Stored procedure parameter values</param>
        public OracleProvider(string connectionString, string storedProcedure, IReadOnlyList<string> parameters,
            IReadOnlyList<object> values)
        {
            _connectionString = connectionString;
            GetData(storedProcedure, parameters, values, null);
        }

        /// <summary>
        /// Returns true if there are still rows in the current table.
        /// Use it with "while"
        /// </summary>
        /// <returns></returns>
        public bool HasRows
        {
            get
            {
                _currentIndex = 0;
                if (_firstRunOnTable)
                {
                    _firstRunOnTable = false;
                    _currentRow = 0;
                }
                else
                    _currentRow++;

                return _currentRow < _data[_currentTable].Rows.Count;
            }
        }

        /// <summary>
        /// Moves up to the next table
        /// </summary>
        public void NextTable()
        {
            _firstRunOnTable = true;
            _currentIndex = 0;
            _currentRow = 0;
            _currentTable++;
        }

        /// <summary>
        /// Executes a stored procedure
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        /// <param name="values">Stored procedure parameter values</param>
        /// <param name="cursors"></param>
        /// <returns></returns>
        private void GetData(string storedProcedure, IReadOnlyList<string> parameters, IReadOnlyList<object> values, IReadOnlyList<string> cursors)
        {
            using (var connection = new OracleConnection(_connectionString))
            {
                using (
                    var command = new OracleCommand(storedProcedure, connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    })
                {
                    if (parameters != null)
                        for (var i = 0; i < parameters.Count; i++)
                        {
                            var parameter = new OracleParameter();
                            parameter.ParameterName = parameters[i];
                            if (values[i] is Enum)
                            {
                                parameter.Value = (int)values[i];
                            }
                            else
                            {
                                parameter.Value = values[i];
                            }

                            if (cursors != null && cursors.Contains(parameter.ParameterName))
                            {
                                parameter.Direction = ParameterDirection.Output;
                                parameter.OracleDbType = OracleDbType.RefCursor;
                            }
                            else
                            {
                                parameter.OracleDbType = GetOracleType(values[i]);
                            }
                            command.Parameters.Add(parameter);
                        }

                    var dataSet = new DataSet();
                    connection.Open();
                    new OracleDataAdapter(command).Fill(dataSet);
                    _data = dataSet.Tables;
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Gets the current value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <returns></returns>
        public T GetValue<T>()
        {
            var isNull = false;
            object value = null;
            try
            {
                value = _data[_currentTable].Rows[_currentRow][_currentIndex];
            }
            catch (Exception)
            {
                if (Nullable.GetUnderlyingType(typeof(T)) != null)
                    isNull = true;
                else
                    throw;
            }
            var valueType = typeof(T).Name;
            _currentIndex++;

            if (isNull)
            {
                return (T) (object) null;
            }

            if (value != DBNull.Value)
            {
                if (valueType == "Boolean")
                {
                    //var booleanValue = (int)value != 0;
                    //return (T)(object)booleanValue;
                    return (T)value;
                }

                if (valueType == "Int32")
                {
                    return (T)(object)int.Parse(value.ToString());
                }

                return (T)value;
            }
            switch (valueType)
            {
                case "String":
                    return (T)(object)null;
                case "Int32":
                    return (T)(object)0;
                case "Boolean":
                    return (T)(object)false;
            }
            throw new Exception("Default null value for object type not supported");
        }

        /// <summary>
        /// Gets the current value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="isEnum">Return type is an enum</param>
        /// <returns></returns>
        public T GetValue<T>(bool isEnum)
        {
            return (T)Enum.Parse(typeof(T), GetValue<int>().ToString(), true);
        }

        public OracleDbType GetOracleType<T>(T item)
        {
            var type = item.GetType();

            if (item is Enum)
                return OracleDbType.Int32;
            if (type == typeof(int))
                return OracleDbType.Int32;
            if (type == typeof(bool))
                return OracleDbType.Int32;
            throw new Exception();
        }

        #region Private

        /// <summary>
        /// Default connection string
        /// </summary>
        private readonly string _defaultConnection = ConfigurationManager.ConnectionStrings["AIT"].ConnectionString;

        private readonly string _connectionString;
        private DataTableCollection _data;
        private int _currentIndex;
        private int _currentRow;
        private int _currentTable;
        private bool _firstRunOnTable = true;

        #endregion
    }
}