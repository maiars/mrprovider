﻿//-----------------------------------------------------------------------
// <copyright file="Connector.cs" company="MaiarPariatur">
// Description  : SQL Server Provider
// Creator      : Maiar Pariatur
// M(aia)rProvider
// </copyright>
//-----------------------------------------------------------------------

namespace MrProvider
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Grants access to a database
    /// </summary>
    public class Connector
    {
        #region Fields

        /// <summary>
        /// Connection String
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// Data tables retrieved by GetData
        /// </summary>
        private DataTableCollection data;

        /// <summary>
        /// Current index or column on the table
        /// </summary>
        private int currentIndex;

        /// <summary>
        /// Current row on the table
        /// </summary>
        private int currentRow;

        /// <summary>
        /// Current table
        /// </summary>
        private int currentTable;

        /// <summary>
        /// Is this the first time we travel this table?
        /// </summary>
        private bool firstRunOnTable = true;

        #endregion

        #region Constructors

        /// <summary>
        /// Negotiates a connection and gets the data with the default connection string, no parameter is used.
        /// Default connection string must be named "MrProvider"
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        public Connector(string storedProcedure)
            : this(
                null,
                storedProcedure,
                null)
        {
        }

        /// <summary>
        /// Negotiates a connection and gets the data with a different connection string, no parameter is used
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        public Connector(string connectionString, string storedProcedure)
            : this(
                connectionString,
                storedProcedure,
                null)
        {
        }

        /// <summary>
        /// Negotiates a connection and gets the data with the default connection string
        /// Default connection string must be named "MrProvider"
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        public Connector(string storedProcedure, IReadOnlyList<Parameter> parameters)
            : this(
                null,
                storedProcedure,
                parameters)
        {
        }

        /// <summary>
        /// Negotiates a connection and gets the data with a different connection string
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        /// <param name="values">Stored procedure parameter values</param>
        public Connector(string connectionString, string storedProcedure, IReadOnlyList<Parameter> parameters)
        {
            // No connection string, try to get default connection string
            if (connectionString == null)
            {
                try
                {
                    this.connectionString = ConfigurationManager.ConnectionStrings["MrProvider"].ConnectionString;
                }
                catch (Exception e)
                {
                    throw new Exception(
                        "Default connection string not found on configuration file. It must be called MrProvider.", e);
                }
            }
            // Defined connection string
            else
            {
                try
                {
                    this.connectionString = connectionString;
                }
                catch (Exception e)
                {
                    throw new Exception("Connection string not found on configuration file.", e);
                }
            }

            GetData(storedProcedure, parameters);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns true if there are still rows in the current table.
        /// Use it with "while"
        /// </summary>
        /// <returns></returns>
        public bool HasRows
        {
            get
            {
                currentIndex = 0;
                if (firstRunOnTable)
                {
                    firstRunOnTable = false;
                    currentRow = 0;
                }
                else
                {
                    currentRow++;
                }

                return currentRow < data[currentTable].Rows.Count;
            }
        }

        /// <summary>
        /// Moves up to the next table
        /// </summary>
        public void NextTable()
        {
            firstRunOnTable = true;
            currentIndex = 0;
            currentRow = 0;
            currentTable++;
        }

        /// <summary>
        /// Gets the current value
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <returns>The asked value</returns>
        public T GetValue<T>()
        {
            var isNull = false;
            object value = null;
            try
            {
                value = data[currentTable].Rows[currentRow][currentIndex];
            }
            catch (Exception)
            {
                if (Nullable.GetUnderlyingType(typeof(T)) != null)
                {
                    isNull = true;
                }
                else
                {
                    throw;
                }
            }

            var valueType = typeof(T).Name;
            currentIndex++;

            if (isNull)
            {
                return (T)(object)null;
            }

            if (value != DBNull.Value)
            {
                return (T)value;
            }

            switch (valueType)
            {
                case "String":
                    return (T)(object)null;
                case "Int32":
                    return (T)(object)0;
                case "Boolean":
                    return (T)(object)false;
            }

            throw new Exception("Default null value for object type not supported");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Executes a stored procedure
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <param name="parameters">Stored procedure parameter names</param>
        private void GetData(string storedProcedure, IReadOnlyList<Parameter> parameters)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (
                    var command = new SqlCommand(storedProcedure, connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    })
                {
                    if (parameters != null)
                    {
                        for (var i = 0; i < parameters.Count; i++)
                        {
                            command.Parameters.AddWithValue("@" + parameters[i].Name,
                                parameters[i].Value ?? DBNull.Value);
                        }
                    }

                    var dataSet = new DataSet();
                    connection.Open();
                    new SqlDataAdapter(command).Fill(dataSet);
                    data = dataSet.Tables;
                    connection.Close();
                }
            }
        }

        #endregion

        #region Classes

        /// <summary>
        /// Represents a parameter which will enter the database through a stored procedure
        /// </summary>
        public class Parameter
        {
            /// <summary>
            /// Intanciates a new Parameter
            /// </summary>
            /// <param name="name"></param>
            /// <param name="value"></param>
            public Parameter(string name, object value)
            {
                Name = name;
                Value = value;
            }

            /// <summary>
            /// The name of the parameter
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// The value of the parameter
            /// </summary>
            public object Value { get; set; }
        }

        #endregion
    }
}